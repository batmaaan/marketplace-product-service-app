package org.marketplace.product.client;

import org.marketplace.product.client.operation.category.CategoryOperation;
import org.marketplace.product.client.operation.product.ProductOperation;

public interface ProductServiceClient {

  CategoryOperation category();

  ProductOperation product();
}
