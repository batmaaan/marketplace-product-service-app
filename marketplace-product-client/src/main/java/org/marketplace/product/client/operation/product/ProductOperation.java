package org.marketplace.product.client.operation.product;

import org.marketplace.product.model.request.ProductRequest;
import org.marketplace.product.model.request.UpdateProductRequest;
import org.marketplace.product.model.response.DeleteResponse;
import org.marketplace.product.model.response.ProductResponse;
import org.marketplace.product.model.response.ProductsListResponse;
import org.marketplace.product.model.utils.Order;

public interface ProductOperation {
  ProductResponse getProductById(Integer id);

  ProductsListResponse getAllProducts(int limit, int offset, Order order);

  ProductsListResponse getProductsByCategory(Integer categoryId, int limit, int offset, Order order);

  ProductResponse saveProduct(ProductRequest productRequest);

  ProductResponse updateProduct(UpdateProductRequest updateProductRequest);

  DeleteResponse deleteById(Integer id, Integer categoryId);


}
