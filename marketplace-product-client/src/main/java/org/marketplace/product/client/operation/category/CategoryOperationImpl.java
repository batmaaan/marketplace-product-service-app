package org.marketplace.product.client.operation.category;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.marketplace.product.model.response.CategoriesListResponse;
import org.marketplace.product.model.response.CategoryResponse;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.marketplace.product.model.utils.UrlEndpoints.GET_ALL_CATEGORIES_URI;
import static org.marketplace.product.model.utils.UrlEndpoints.GET_CATEGORY_BY_ID_URI;


public class CategoryOperationImpl implements CategoryOperation {
  ObjectMapper objectMapper = new ObjectMapper();
  private final HttpClient httpClient;
  private final String URL;


  public CategoryOperationImpl(HttpClient httpClient, String url) {
    this.httpClient = httpClient;
    this.URL = url;
  }

  @Override
  @SneakyThrows
  public CategoryResponse getCategoryById(Integer id) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + GET_CATEGORY_BY_ID_URI + "?id=" + id))
      .GET()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), CategoryResponse.class);
  }

  @SneakyThrows
  @Override
  public CategoriesListResponse getAllCategories() {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + GET_ALL_CATEGORIES_URI))
      .GET()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), CategoriesListResponse.class);
  }
}
