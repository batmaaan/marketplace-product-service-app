package org.marketplace.product.client.operation.product;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.marketplace.product.model.request.ProductRequest;
import org.marketplace.product.model.request.UpdateProductRequest;
import org.marketplace.product.model.response.DeleteResponse;
import org.marketplace.product.model.response.ProductResponse;
import org.marketplace.product.model.response.ProductsListResponse;
import org.marketplace.product.model.utils.Order;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import static org.marketplace.product.model.utils.UrlEndpoints.*;

public class ProductOperationImpl implements ProductOperation {
  ObjectMapper objectMapper = new ObjectMapper();
  private final HttpClient httpClient;
  private final String URL;


  public ProductOperationImpl(HttpClient httpClient, String url) {
    this.httpClient = httpClient;
    this.URL = url;
  }


  @Override
  @SneakyThrows
  public ProductResponse getProductById(Integer id) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + GET_PRODUCT_BY_ID + "?id=" + id))
      .GET()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), ProductResponse.class);
  }

  @Override
  @SneakyThrows
  public ProductsListResponse getAllProducts(int limit, int offset, Order order) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + GET_ALL_PRODUCTS + "?" + "limit=" + limit + "&offset=" + offset + "&order=" + order.toString()))
      .GET()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), ProductsListResponse.class);
  }

  @Override
  @SneakyThrows
  public ProductsListResponse getProductsByCategory(Integer categoryId, int limit, int offset, Order order) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + GET_PRODUCT_BY_CATEGORY + "?categoryId=" + categoryId + "&limit=" + limit + "&offset=" + offset + "&order=" + order.toString()))
      .GET()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), ProductsListResponse.class);
  }

  @Override
  @SneakyThrows
  public ProductResponse saveProduct(ProductRequest productRequest) {
    String json = objectMapper.writeValueAsString(productRequest);
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + SAVE_PRODUCT_URI))
      .timeout(Duration.ofSeconds(5))
      .header("Content-Type", "application/json")
      .POST(HttpRequest.BodyPublishers.ofString(json))
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), ProductResponse.class);
  }

  @Override
  @SneakyThrows
  public ProductResponse updateProduct(UpdateProductRequest updateProductRequest) {
    String json = objectMapper.writeValueAsString(updateProductRequest);
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + UPDATE_PRODUCT_URI))
      .timeout(Duration.ofSeconds(5))
      .header("Content-Type", "application/json")
      .POST(HttpRequest.BodyPublishers.ofString(json))
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), ProductResponse.class);
  }

  @Override
  @SneakyThrows
  public DeleteResponse deleteById(Integer id, Integer categoryId) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + DELETE_PRODUCT_BY_ID_URI + "?id=" + id +"&categoryId="+ categoryId))
      .DELETE()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), DeleteResponse.class);
  }
}
