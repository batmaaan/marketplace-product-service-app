package org.marketplace.product.client.operation.category;

import org.marketplace.product.model.response.CategoriesListResponse;
import org.marketplace.product.model.response.CategoryResponse;

public interface CategoryOperation {
  CategoryResponse getCategoryById(Integer id);
  CategoriesListResponse getAllCategories();
}
