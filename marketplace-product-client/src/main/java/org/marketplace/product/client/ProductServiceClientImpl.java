package org.marketplace.product.client;

import org.marketplace.product.client.operation.category.CategoryOperation;
import org.marketplace.product.client.operation.category.CategoryOperationImpl;
import org.marketplace.product.client.operation.product.ProductOperation;
import org.marketplace.product.client.operation.product.ProductOperationImpl;

import java.net.http.HttpClient;
import java.time.Duration;

public class ProductServiceClientImpl implements ProductServiceClient {

  private final CategoryOperation categoryOperation;

  private final ProductOperation productOperation;

  public ProductServiceClientImpl(String url) {
    HttpClient client = HttpClient.newBuilder()
      .version(HttpClient.Version.HTTP_1_1)
      .followRedirects(HttpClient.Redirect.NORMAL)
      .connectTimeout(Duration.ofSeconds(5))
      .build();

    this.categoryOperation = new CategoryOperationImpl(client, url);
    this.productOperation = new ProductOperationImpl(client, url);
  }

  @Override
  public CategoryOperation category() {
    return categoryOperation;
  }

  @Override
  public ProductOperation product() {
    return productOperation;
  }
}
