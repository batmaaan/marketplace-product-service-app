package org.marketplace.product.service.data.service;

import org.junit.jupiter.api.Test;
import org.marketplace.product.model.request.ProductRequest;
import org.marketplace.product.model.request.UpdateProductRequest;
import org.marketplace.product.model.response.DeleteResponse;
import org.marketplace.product.model.response.ProductResponse;
import org.marketplace.product.model.response.ProductsListResponse;
import org.marketplace.product.model.utils.Order;
import org.marketplace.product.service.AbstractIntegrationTest;
import org.marketplace.utils.InitProduct;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

import java.util.ArrayList;

class ProductServiceTest extends AbstractIntegrationTest {
  @Autowired
  private ProductService productService;


  @Test
  void saveAndGetProductById() {
    var randomProduct = InitProduct.initRandomProduct();
    var savedProduct = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    ));
    var result = savedProduct
      .map(productResponse -> productResponse.getProduct().getId())
      .flatMap(id -> productService.getProductById(id));

    StepVerifier.create(result.map(ProductResponse::getStatus))
      .expectNext("ok")
      .verifyComplete();
    StepVerifier.create(result.map(ProductResponse::getDescription))
      .expectNext("")
      .verifyComplete();
    StepVerifier.create(result.map(productResponse -> productResponse.getProduct().getCategoryId()))
      .expectNext(randomProduct.getCategoryId())
      .verifyComplete();
    StepVerifier.create(result.map(productResponse -> productResponse.getProduct().getProductName()))
      .expectNext(randomProduct.getProductName())
      .verifyComplete();
    StepVerifier.create(result.map(productResponse -> productResponse.getProduct().getPrice()))
      .expectNext(randomProduct.getPrice())
      .verifyComplete();
  }

  @Test
  void getProductByZeroId() {

    StepVerifier.create(productService.getProductById(0))
      .expectNext(new ProductResponse(
        "error",
        "incorrect id",
        null))
      .verifyComplete();

  }

  @Test
  void getProductByMinusId() {
    StepVerifier.create(productService.getProductById(-12))
      .expectNext(new ProductResponse(
        "error",
        "incorrect id",
        null))
      .verifyComplete();
  }

  @Test
  void getAllProductsHasNextTrue() {
    var randomProduct = InitProduct.initRandomProduct();
    var result = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    )).flatMap(productResponse -> productService.getAllProducts(0, 0, Order.DESC));


    StepVerifier.create(result)
      .expectNext(new ProductsListResponse("ok",
        "",
        true,
        new ArrayList<>()
      ))
      .verifyComplete();

  }

  @Test
  void getAllProductsHasNextFalse() {
    StepVerifier.create(productService.getAllProducts(500, 0, Order.DESC))
      .expectNext(new ProductsListResponse("ok",
        "",
        false,
        new ArrayList<>()
      ))
      .verifyComplete();
  }

  @Test
  void getProductsByCategoryHasNextTrue() {
    var randomProduct = InitProduct.initRandomProduct();
    var result = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    )).flatMap(productResponse -> productService.getProductsByCategory(randomProduct.getCategoryId(), 0, 0, Order.DESC));

    StepVerifier.create(result)
      .expectNext(new ProductsListResponse("ok",
        "",
        true,
        new ArrayList<>()
      ))
      .verifyComplete();
  }

  @Test
  void getProductsByCategoryHasNextFalse() {
    var randomProduct = InitProduct.initRandomProduct();
    var result = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    )).flatMap(productResponse -> productService.getProductsByCategory(randomProduct.getCategoryId(), 500, 1, Order.DESC));

    StepVerifier.create(result)
      .expectNext(new ProductsListResponse("ok",
        "",
        false,
        new ArrayList<>()
      ))
      .verifyComplete();
  }

  @Test
  void getProductsWithWrongCategoryId() {

    var productsByCategory = productService.getProductsByCategory(0, 5, 0, Order.DESC);

    StepVerifier.create(productsByCategory)
      .expectNext(new ProductsListResponse("error",
        "incorrect category ID",
        false,
        null
      ))
      .verifyComplete();
  }

  @Test
  void saveProductWithNullCategory() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = new ProductRequest(
      null,
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    );

    StepVerifier.create(productService.saveProduct(newProduct))
      .expectNext(new ProductResponse(
        "error",
        "incorrect category ID",
        null))
      .verifyComplete();
  }

  @Test
  void saveProductWithNullName() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = new ProductRequest(
      randomProduct.getCategoryId(),
      null,
      randomProduct.getDescription(),
      randomProduct.getPrice()
    );

    StepVerifier.create(productService.saveProduct(newProduct))
      .expectNext(new ProductResponse(
        "error",
        "product name must be not empty",
        null))
      .verifyComplete();
  }

  @Test
  void saveProductWithNullDescription() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      null,
      randomProduct.getPrice()
    );
    StepVerifier.create(productService.saveProduct(newProduct))
      .expectNext(new ProductResponse(
        "error",
        "description must be not empty",
        null))
      .verifyComplete();
  }

  @Test
  void updateProduct() {
    var randomProduct = InitProduct.initRandomProduct();
    var randomForUpdate = InitProduct.initRandomProduct();
    var result = productService.saveProduct(new ProductRequest(
        randomProduct.getCategoryId(),
        randomProduct.getProductName(),
        randomProduct.getDescription(),
        randomProduct.getPrice()
      )).map(productResponse -> productResponse.getProduct().getId())
      .flatMap(id -> {
        var updatedProductRequest = new UpdateProductRequest(
          id,
          randomForUpdate.getCategoryId(),
          randomForUpdate.getProductName(),
          randomForUpdate.getDescription(),
          randomForUpdate.getPrice());
        return productService.updateProduct(updatedProductRequest);
      });

    StepVerifier.create(result.map(ProductResponse::getStatus))
      .expectNext("ok")
      .verifyComplete();
    StepVerifier.create(result.map(ProductResponse::getDescription))
      .expectNext("product updated")
      .verifyComplete();
    StepVerifier.create(result.map(productResponse -> productResponse.getProduct().getCategoryId()))
      .expectNext(randomForUpdate.getCategoryId())
      .verifyComplete();
    StepVerifier.create(result.map(productResponse -> productResponse.getProduct().getProductName()))
      .expectNext(randomForUpdate.getProductName())
      .verifyComplete();
    StepVerifier.create(result.map(productResponse -> productResponse.getProduct().getPrice()))
      .expectNext(randomForUpdate.getPrice())
      .verifyComplete();
  }

  @Test
  void updateProductWithNullCategory() {

    var randomProduct = InitProduct.initRandomProduct();
    var randomForUpdate = InitProduct.initRandomProduct();
    var result = productService.saveProduct(new ProductRequest(
        randomProduct.getCategoryId(),
        randomProduct.getProductName(),
        randomProduct.getDescription(),
        randomProduct.getPrice()
      )).map(productResponse -> productResponse.getProduct().getId())
      .flatMap(id -> {
        var updatedProductRequest = new UpdateProductRequest(
          id,
          null,
          randomForUpdate.getProductName(),
          randomForUpdate.getDescription(),
          randomForUpdate.getPrice());
        return productService.updateProduct(updatedProductRequest);
      });
    StepVerifier.create(result)
      .expectNext(
        new ProductResponse(
          "error",
          "incorrect category ID",
          null))
      .verifyComplete();
  }

  @Test
  void updateProductWithNullName() {
    var randomProduct = InitProduct.initRandomProduct();
    var randomForUpdate = InitProduct.initRandomProduct();
    var result = productService.saveProduct(new ProductRequest(
        randomProduct.getCategoryId(),
        randomProduct.getProductName(),
        randomProduct.getDescription(),
        randomProduct.getPrice()
      )).map(productResponse -> productResponse.getProduct().getId())
      .flatMap(id -> {
        var updatedProductRequest = new UpdateProductRequest(
          id,
          randomForUpdate.getCategoryId(),
          null,
          randomForUpdate.getDescription(),
          randomForUpdate.getPrice());
        return productService.updateProduct(updatedProductRequest);
      });
    StepVerifier.create(result)
      .expectNext(
        new ProductResponse(
          "error",
          "product name must be not empty",
          null))
      .verifyComplete();
  }

  @Test
  void updateProductWithNullDescription() {
    var randomProduct = InitProduct.initRandomProduct();
    var randomForUpdate = InitProduct.initRandomProduct();
    var result = productService.saveProduct(new ProductRequest(
        randomProduct.getCategoryId(),
        randomProduct.getProductName(),
        randomProduct.getDescription(),
        randomProduct.getPrice()
      )).map(productResponse -> productResponse.getProduct().getId())
      .flatMap(id -> {
        var updatedProductRequest = new UpdateProductRequest(
          id,
          randomForUpdate.getCategoryId(),
          randomForUpdate.getProductName(),
          null,
          randomForUpdate.getPrice());
        return productService.updateProduct(updatedProductRequest);
      });
    StepVerifier.create(result)
      .expectNext(
        new ProductResponse(
          "error",
          "description must be not empty",
          null))
      .verifyComplete();
  }

  @Test
  void deleteById() {
    var randomProduct = InitProduct.initRandomProduct();
    var product = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    ));
    var delete = product.map(productResponse -> productResponse.getProduct().getId())
      .flatMap(id -> productService.deleteById(id, randomProduct.getCategoryId()));

    StepVerifier.create(delete)
      .expectNext(new DeleteResponse("ok", "product deleted"))
      .verifyComplete();
  }

  @Test
  void deleteByWrongId() {
    StepVerifier.create(productService.deleteById(99999, 99))
      .expectNext(new DeleteResponse("error", "product not found"))
      .verifyComplete();
  }
}
