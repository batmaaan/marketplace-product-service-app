package org.marketplace.product.service.data.service;

import org.junit.jupiter.api.Test;
import org.marketplace.product.model.response.CategoriesListResponse;
import org.marketplace.product.model.response.CategoryResponse;
import org.marketplace.product.service.AbstractIntegrationTest;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

import java.util.Random;

class CategoryServiceTest extends AbstractIntegrationTest {

  @Autowired
  private CategoryService categoryService;

  @Test
  void getCategoryById() {
    Random random = new Random();
    var randomCategory = random.nextInt(9) + 1;

    StepVerifier.create(categoryService.getCategoryById(randomCategory).map(CategoryResponse::getStatus))
      .expectNext(
        "ok")
      .verifyComplete();

  }

  @Test
  void getCategoryWithZeroId() {
    StepVerifier.create(categoryService.getCategoryById(0))
      .expectNext(new CategoryResponse(
        "error",
        "incorrect id",
        null))
      .verifyComplete();
  }

  @Test
  void getCategoryWithMinusId() {
    StepVerifier.create(categoryService.getCategoryById(-4))
      .expectNext(new CategoryResponse(
        "error",
        "incorrect id",
        null))
      .verifyComplete();
  }

  @Test
  void getAllCategories() {
    StepVerifier.create(categoryService.getAllCategories().map(CategoriesListResponse::getStatus))
      .expectNext("ok")
      .verifyComplete();

  }

}
