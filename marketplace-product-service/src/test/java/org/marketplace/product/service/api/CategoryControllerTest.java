package org.marketplace.product.service.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.marketplace.product.client.ProductServiceClient;
import org.marketplace.product.client.ProductServiceClientImpl;
import org.marketplace.product.service.AbstractIntegrationTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;


class CategoryControllerTest extends AbstractIntegrationTest {
  @LocalServerPort
  int randomServerPort;

  private ProductServiceClient client;


  @BeforeEach
  public void init() {
    if (client == null) {
      this.client = new ProductServiceClientImpl("http://localhost:" + randomServerPort);
    }
  }

  @Test
  void getCategoryById() {
    Random random = new Random();
    var randomCategory = random.nextInt(9) + 1;
    var category = client.category().getCategoryById(randomCategory);
    assertEquals("ok", category.getStatus());
    assertEquals(randomCategory, category.getCategory().getId());
  }

  @Test
  void getCategoryWithIncorrectId() {

    var result = client.category().getCategoryById(1000);

    assertEquals("error", result.getStatus());
    assertEquals("incorrect id", result.getDescription());
    assertNull(result.getCategory());

  }

  @Test
  void getCategoryWithZeroId() {
    var result = client.category().getCategoryById(0);

    assertEquals("error", result.getStatus());
    assertEquals("incorrect id", result.getDescription());
    assertNull(result.getCategory());

  }

  @Test
  void getAllCategories() {
    var result = client.category().getAllCategories();
    assertEquals("ok", result.getStatus());
    assertNotNull(result.getCategories());
  }
}

