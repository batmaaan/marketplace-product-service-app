package org.marketplace.product.service.api;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.marketplace.product.client.ProductServiceClient;
import org.marketplace.product.client.ProductServiceClientImpl;
import org.marketplace.product.model.request.ProductRequest;
import org.marketplace.product.model.request.UpdateProductRequest;
import org.marketplace.product.model.response.ProductResponse;
import org.marketplace.product.model.utils.Order;
import org.marketplace.product.service.AbstractIntegrationTest;
import org.marketplace.product.service.data.service.ProductService;
import org.marketplace.utils.InitProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class ProductControllerTest extends AbstractIntegrationTest {
  @LocalServerPort
  int randomServerPort;
  @Autowired
  private ProductService productService;
  private ProductServiceClient client;


  @BeforeEach
  public void init() {
    if (client == null) {
      this.client = new ProductServiceClientImpl("http://localhost:" + randomServerPort);

    }
  }

//  @Test
//  void getProductById() {
//    var randomProduct = InitProduct.initRandomProduct();
//    var product = productService.saveProduct(new ProductRequest(
//      randomProduct.getCategoryId(),
//      randomProduct.getProductName(),
//      randomProduct.getDescription(),
//      randomProduct.getPrice()
//    ));
//    var result = client.product().getProductById(product.getProduct().getId());
//
//    assertEquals("ok", result.getStatus());
//    assertNotNull(result.getProduct());
//
//  }


  @Test
  void getProductByZeroId() {
    var result = client.product().getProductById(0);
    assertEquals("error", result.getStatus());
    assertEquals("incorrect id", result.getDescription());
    assertNull(result.getProduct());
  }

  @Test
  void getProductByMinusId() {
    var result = client.product().getProductById(-12);
    assertEquals("error", result.getStatus());
    assertEquals("incorrect id", result.getDescription());
    assertNull(result.getProduct());

  }

  @Test
  void getAllProductsHasNextTrue() {
    var randomProduct = InitProduct.initRandomProduct();
    for (int i = 0; i < 10; i++) {
      productService.saveProduct(new ProductRequest(
        randomProduct.getCategoryId(),
        randomProduct.getProductName(),
        randomProduct.getDescription(),
        randomProduct.getPrice()
      )).share().block();
    }
    Random random = new Random();
    var limit = random.nextInt(4) + 1;
    var offset = random.nextInt(4) + 1;
    var result = client.product().getAllProducts(limit, offset, Order.DESC);

    assertEquals("ok", result.getStatus());
    assertTrue(result.isHasNext());
    assertNotNull(result.getProducts());
  }

  @Test
  void getAllProductsHasNextFalse() {
    var randomProduct = InitProduct.initRandomProduct();
    for (int i = 0; i < 10; i++) {
      productService.saveProduct(new ProductRequest(
        randomProduct.getCategoryId(),
        randomProduct.getProductName(),
        randomProduct.getDescription(),
        randomProduct.getPrice()
      )).share().block();
    }
    var result = client.product().getAllProducts(500, 0, Order.DESC);

    assertEquals("ok", result.getStatus());
    assertFalse(result.isHasNext());
    assertNotNull(result.getProducts());
  }


  @Test
  void getProductsByCategoryHasNextTrue() {
    var randomProduct = InitProduct.initRandomProduct();
    for (int i = 0; i < 10; i++) {
      productService.saveProduct(new ProductRequest(
        randomProduct.getCategoryId(),
        randomProduct.getProductName(),
        randomProduct.getDescription(),
        randomProduct.getPrice()
      )).share().block();
    }
    Random random = new Random();
    var limit = random.nextInt(4 + 1);
    var offset = random.nextInt(4 + 1);
    var result = client.product().getProductsByCategory(randomProduct.getCategoryId(), limit, offset, Order.DESC);

    assertEquals("ok", result.getStatus());
    assertTrue(result.isHasNext());
    assertNotNull(result.getProducts());

  }

  @Test
  void getProductsByCategoryHasNextFalse() {
    var randomProduct = InitProduct.initRandomProduct();
    for (int i = 0; i < 10; i++) {
      productService.saveProduct(new ProductRequest(
        randomProduct.getCategoryId(),
        randomProduct.getProductName(),
        randomProduct.getDescription(),
        randomProduct.getPrice()
      )).share().block();
    }

    var result = client.product().getProductsByCategory(randomProduct.getCategoryId(), 500, 0, Order.DESC);

    assertEquals("ok", result.getStatus());
    assertFalse(result.isHasNext());
    assertNotNull(result.getProducts());

  }

  @Test
  void getProductsWithWrongCategoryId() {


    Random random = new Random();
    var limit = random.nextInt(4 + 1);
    var offset = random.nextInt(4 + 1);
    var result = client.product().getProductsByCategory(0, limit, offset, Order.DESC);

    assertEquals("error", result.getStatus());
    assertEquals("incorrect category ID", result.getDescription());
    assertFalse(result.isHasNext());
    assertNull(result.getProducts());

  }

  @Test
  void saveProduct() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    );
    var result = client.product().saveProduct(newProduct);

    assertEquals("ok", result.getStatus());
    assertEquals(randomProduct.getCategoryId(), result.getProduct().getCategoryId());
    assertEquals(randomProduct.getProductName(), result.getProduct().getProductName());
    assertEquals(randomProduct.getDescription(), result.getProduct().getDescription());
    assertEquals(randomProduct.getPrice(), result.getProduct().getPrice());

  }

  @Test
  void saveProductWithNullCategory() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = new ProductRequest(
      null,
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    );
    var errorResponse = new ProductResponse("error", "incorrect category ID", null);
    var result = client.product().saveProduct(newProduct);

    assertEquals(errorResponse.getStatus(), result.getStatus());
    assertEquals(errorResponse.getDescription(), result.getDescription());
    assertNull(result.getProduct());

  }

  @Test
  void saveProductWithNullName() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = new ProductRequest(
      randomProduct.getCategoryId(),
      null,
      randomProduct.getDescription(),
      randomProduct.getPrice()
    );
    var errorResponse = new ProductResponse("error", "product name must be not empty", null);
    var result = client.product().saveProduct(newProduct);

    assertEquals(errorResponse.getStatus(), result.getStatus());
    assertEquals(errorResponse.getDescription(), result.getDescription());
    assertNull(result.getProduct());
  }

  @Test
  void saveProductWithNullDescription() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      null,
      randomProduct.getPrice()
    );
    var errorResponse = new ProductResponse("error", "description must be not empty", null);
    var result = client.product().saveProduct(newProduct);

    assertEquals(errorResponse.getStatus(), result.getStatus());
    assertEquals(errorResponse.getDescription(), result.getDescription());
    assertNull(result.getProduct());

  }


  @Test
  void updateProduct() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    )).share().block();
    var randomForUpdate = InitProduct.initRandomProduct();
    assert newProduct != null;
    var updatedProductRequest = new UpdateProductRequest(
      newProduct.getProduct().getId(),
      randomForUpdate.getCategoryId(),
      randomForUpdate.getProductName(),
      randomForUpdate.getDescription(),
      randomForUpdate.getPrice()
    );
    var result = client.product().updateProduct(updatedProductRequest);
    assertEquals("ok", result.getStatus());
    assertEquals(randomForUpdate.getCategoryId(), result.getProduct().getCategoryId());
    assertEquals(randomForUpdate.getProductName(), result.getProduct().getProductName());
    assertEquals(randomForUpdate.getDescription(), result.getProduct().getDescription());
    assertEquals(randomForUpdate.getPrice(), result.getProduct().getPrice());
  }

  @Test
  void updateProductWithNullCategory() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    )).share().block();
    var randomForUpdate = InitProduct.initRandomProduct();
    assert newProduct != null;
    var updatedProductRequest = new UpdateProductRequest(
      newProduct.getProduct().getId(),
      null,
      randomForUpdate.getProductName(),
      randomForUpdate.getDescription(),
      randomForUpdate.getPrice()
    );

    var errorResponse = new ProductResponse("error", "incorrect category ID", null);
    var result = client.product().updateProduct(updatedProductRequest);

    assertEquals(errorResponse.getStatus(), result.getStatus());
    assertEquals(errorResponse.getDescription(), result.getDescription());
    assertNull(result.getProduct());

  }

  @Test
  void updateProductWithNullName() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    )).share().block();
    var randomForUpdate = InitProduct.initRandomProduct();
    assert newProduct != null;
    var updatedProductRequest = new UpdateProductRequest(
      newProduct.getProduct().getId(),
      newProduct.getProduct().getCategoryId(),
      null,
      randomForUpdate.getDescription(),
      randomForUpdate.getPrice()
    );
    var errorResponse = new ProductResponse("error", "product name must be not empty", null);
    var result = client.product().updateProduct(updatedProductRequest);

    assertEquals(errorResponse.getStatus(), result.getStatus());
    assertEquals(errorResponse.getDescription(), result.getDescription());
    assertNull(result.getProduct());
  }

  @Test
  void updateProductWithNullDescription() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    )).share().block();
    var randomForUpdate = InitProduct.initRandomProduct();
    assert newProduct != null;
    var updatedProductRequest = new UpdateProductRequest(
      newProduct.getProduct().getId(),
      newProduct.getProduct().getCategoryId(),
      randomForUpdate.getProductName(),
      null,
      randomForUpdate.getPrice()
    );
    var errorResponse = new ProductResponse("error", "description must be not empty", null);
    var result = client.product().updateProduct(updatedProductRequest);

    assertEquals(errorResponse.getStatus(), result.getStatus());
    assertEquals(errorResponse.getDescription(), result.getDescription());
    assertNull(result.getProduct());
  }

  @Test
  void deleteById() {
    var randomProduct = InitProduct.initRandomProduct();
    var newProduct = productService.saveProduct(new ProductRequest(
      randomProduct.getCategoryId(),
      randomProduct.getProductName(),
      randomProduct.getDescription(),
      randomProduct.getPrice()
    )).share().block();
    assert newProduct != null;
    var result = client.product().deleteById(newProduct.getProduct().getId(), randomProduct.getCategoryId());
    assertEquals("ok", result.getStatus());

  }

  @Test
  void deleteByWrongId() {
    var result = client.product().deleteById(9999, 99);
    assertEquals("error", result.getStatus());
  }
}
