package org.marketplace.product.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(
  initializers = {AbstractIntegrationTest.Initializer.class
  }
)

public abstract class AbstractIntegrationTest {

  @Container
  public static final PostgreSQLContainer<?> POSTGRES = new PostgreSQLContainer<>("postgres:12")
    .withInitScript("db/init_test_db.sql");


  static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public void initialize(ConfigurableApplicationContext applicationContext) {
      POSTGRES.start();

      var testPropertyValues = TestPropertyValues.of(
        "app.datasource.host=" + POSTGRES.getHost(),
        "app.datasource.username=" + POSTGRES.getUsername(),
        "app.datasource.password=" + POSTGRES.getPassword(),
        "app.datasource.database=" + POSTGRES.getDatabaseName(),
        "app.datasource.port=" + POSTGRES.getMappedPort(5432)
      );
      testPropertyValues.applyTo(applicationContext.getEnvironment());
    }
  }
}


