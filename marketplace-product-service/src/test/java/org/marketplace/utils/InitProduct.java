package org.marketplace.utils;

import org.marketplace.product.model.Product;
import org.testcontainers.shaded.org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;


public class InitProduct {

  public static Product initRandomProduct() {
    Random random = new Random();
    return Product.builder()
      .id(null)
      .productName(RandomStringUtils.randomAlphabetic(5))
      .categoryId(random.nextInt(10) + 1)
      .description(RandomStringUtils.randomAlphabetic(5))
      .price(random.nextInt(100) + 1)
      .build();
  }


}
