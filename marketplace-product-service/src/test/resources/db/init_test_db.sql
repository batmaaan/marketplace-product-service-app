create sequence products_id_seq;
create table products

(
  id           integer default nextval('products_id_seq'::regclass) not null
    primary key,
  category_id   integer,
  product_name varchar(500)                                         not null,
  description  varchar(500)                                         not null,
  price        integer                                              not null
);

create sequence categories_id_seq;
create table categories

(
  id            integer default nextval('categories_id_seq'::regclass) not null
    primary key,
  category_name varchar(100)                                           not null,
  description   varchar(100)                                           not null
);

INSERT INTO categories (category_name, description)
VALUES ('PHONES', 'ALL PHONES IN THE WORLD');
INSERT INTO categories (category_name, description)
VALUES ('TV', 'ALL TV`S IN THE WORLD');
INSERT INTO categories (category_name, description)
VALUES ('GADGETS', 'ALL GADGETS IN THE WORLD');
INSERT INTO categories (category_name, description)
VALUES ('TABLEWARE', 'ALL TABLEWARE IN THE WORLD');
INSERT INTO categories (category_name, description)
VALUES ('LARGE HOME APPLIANCES', 'LARGE HOME APPLIANCES');
INSERT INTO categories (category_name, description)
VALUES ('PHONES', 'ALL PHONES IN THE WORLD');
INSERT INTO categories (category_name, description)
VALUES ('FURNITURE', 'BETTER THAN IKEA');
INSERT INTO categories (category_name, description)
VALUES ('PRESENTS', 'BEST PRESENTS');
INSERT INTO categories (category_name, description)
VALUES ('CAR GOODS', 'ALL CAR GOODS IN THE WORLD');

