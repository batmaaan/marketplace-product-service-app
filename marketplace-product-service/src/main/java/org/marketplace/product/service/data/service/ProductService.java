package org.marketplace.product.service.data.service;

import lombok.AllArgsConstructor;
import org.marketplace.product.model.request.ProductRequest;
import org.marketplace.product.model.request.UpdateProductRequest;
import org.marketplace.product.model.response.DeleteResponse;
import org.marketplace.product.model.response.ProductResponse;
import org.marketplace.product.model.response.ProductsListResponse;
import org.marketplace.product.model.utils.Order;
import org.marketplace.product.service.data.repository.ProductRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor

public class ProductService {

  private final ProductRepository productRepository;

  private static final String ERROR = "error";
  private static final String INCORRECT_CATEGORY = "incorrect category ID";
  private static final String OK = "ok";
  private static final Mono<ProductsListResponse> INCORRECT_CATEGORY_RESPONSE = Mono.just(new ProductsListResponse(
    ERROR,
    INCORRECT_CATEGORY,
    false,
    null
  ));

  private static final Mono<ProductResponse> INCORRECT_ID_RESPONSE = Mono.just(new ProductResponse(
    ERROR,
    "incorrect id",
    null
  ));


  public Mono<ProductResponse> getProductById(Integer id) {

    if (id == 0 || id <= 0) {
      return INCORRECT_ID_RESPONSE;

    } else {
      return productRepository.getProductById(id).map(product -> new ProductResponse(OK, "", product));
    }
  }


  public Mono<ProductsListResponse> getAllProducts(int limit, int offset, Order order) {
    return productRepository.getAllProducts(limit + 1, offset, order)
      .collectList()
      .map(list -> {
          if (list.size() > limit) {
            return new ProductsListResponse(
              OK,
              "",
              true,
              list.stream().limit(limit).toList()
            );
          } else {
            return new ProductsListResponse(
              OK,
              "",
              false,
              list.stream().limit(limit).toList()
            );
          }
        }
      );
  }

  public Mono<ProductsListResponse> getProductsByCategory(Integer categoryId, int limit, int offset, Order order) {

    if (categoryId <= 0 || categoryId > 9) {
      return INCORRECT_CATEGORY_RESPONSE;
    } else {
      return productRepository.getProductsByCategory(categoryId, limit + 1, offset, order)
        .collectList()
        .map(list -> {
            if (list.size() > limit) {
              return new ProductsListResponse(
                OK,
                "",
                true,
                list.stream().limit(limit).toList()
              );
            } else {
              return new ProductsListResponse(
                OK,
                "",
                false,
                list.stream().limit(limit).toList()
              );
            }
          }
        );
    }
  }

  public Mono<ProductResponse> saveProduct(ProductRequest productRequest) {

    if (productRequest.getCategoryId() == null) {
      return Mono.just(new ProductResponse(
        ERROR,
        INCORRECT_CATEGORY,
        null
      ));
    }
    if (productRequest.getProductName() == null || productRequest.getProductName().length() < 5) {
      return Mono.just(new ProductResponse(
        ERROR,
        "product name must be not empty",
        null
      ));
    }
    if (productRequest.getDescription() == null || productRequest.getDescription().length() < 5) {
      return Mono.just(new ProductResponse(
        ERROR,
        "description must be not empty",
        null
      ));
    } else {
      return productRepository.saveProduct(productRequest)
        .map(product -> new ProductResponse(
          OK,
          "product saved",
          product
        ));
    }
  }

  public Mono<ProductResponse> updateProduct(UpdateProductRequest updateProductRequest) {

    if (updateProductRequest.getId() == null) {
      return Mono.just(new ProductResponse(ERROR, "invalid id", null));
    }
    if (updateProductRequest.getCategoryId() == null) {
      return Mono.just(new ProductResponse(
        ERROR,
        INCORRECT_CATEGORY,
        null
      ));
    }
    if (updateProductRequest.getProductName() == null || updateProductRequest.getProductName().length() < 5) {
      return Mono.just(new ProductResponse(
        ERROR,
        "product name must be not empty",
        null
      ));
    } else if (updateProductRequest.getDescription() == null || updateProductRequest.getDescription().length() < 5) {
      return Mono.just(new ProductResponse(
        ERROR,
        "description must be not empty",
        null
      ));
    } else {
      return productRepository.updateProduct(updateProductRequest).map(
        product -> new ProductResponse(
          OK,
          "product updated",
          product
        )
      );
    }
  }

  public Mono<DeleteResponse> deleteById(Integer id, Integer categoryId) {
    if (id <= 0) {
      return Mono.just(new DeleteResponse(ERROR, "product not found"));
    } else {
      return productRepository.deleteById(id, categoryId).flatMap(rowsUpdated -> {
        if (rowsUpdated != 1) {
          return Mono.just(new DeleteResponse(ERROR, "product not found"));
        } else return Mono.just(new DeleteResponse(OK, "product deleted"));
      });
    }
  }
}

