package org.marketplace.product.service.api;

import lombok.AllArgsConstructor;
import org.marketplace.product.model.response.CategoriesListResponse;
import org.marketplace.product.model.response.CategoryResponse;
import org.marketplace.product.service.data.service.CategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import static org.marketplace.product.model.utils.UrlEndpoints.GET_ALL_CATEGORIES_URI;
import static org.marketplace.product.model.utils.UrlEndpoints.GET_CATEGORY_BY_ID_URI;

@RestController
@AllArgsConstructor
public class CategoryController {

  private final CategoryService categoryService;

  @GetMapping(value = GET_CATEGORY_BY_ID_URI)
  public Mono<CategoryResponse> getCategoryById(@RequestParam Integer id) {
    return categoryService.getCategoryById(id);
  }

  @GetMapping(value = GET_ALL_CATEGORIES_URI)
  public Mono<CategoriesListResponse> getAllCategories() {
    return categoryService.getAllCategories();
  }
}


