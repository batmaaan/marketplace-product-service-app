package org.marketplace.product.service.data.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.marketplace.product.model.Category;
import org.marketplace.product.model.response.CategoriesListResponse;
import org.marketplace.product.model.response.CategoryResponse;
import org.marketplace.product.service.data.repository.CategoryRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@AllArgsConstructor
@Slf4j
public class CategoryService {

  private static Flux<Category> ALL_CATEGORIES = Flux.just();
  public static final String ERROR = "error";
  public static final String OK = "ok";
  public static final String INCORRECT_ID = "incorrect id";
  public static final Mono<CategoryResponse> INCORRECT_ID_RESPONSE = Mono.just(new CategoryResponse(
    ERROR,
    INCORRECT_ID,
    null
  ));


  private final CategoryRepository categoryRepository;

  public Mono<CategoryResponse> getCategoryById(Integer id) {
    if (id == null || id > 9) {
      return INCORRECT_ID_RESPONSE;
    }
    if (id == 0 || id <= 0) {
      return INCORRECT_ID_RESPONSE;
    } else {
      return categoryRepository.getCategoryById(id).map(category -> new CategoryResponse(
        OK,
        "",
        category));
    }
  }

  public Mono<CategoriesListResponse> getAllCategories() {
    return ALL_CATEGORIES.collectList().map(list -> new CategoriesListResponse(OK, list.stream().toList()));
  }

  @PostConstruct()
  public void initCategories() {
    var executor = Executors.newSingleThreadScheduledExecutor();
    executor.scheduleWithFixedDelay(() -> {
      var list = categoryRepository.getAllCategories();
      ALL_CATEGORIES = null;
      ALL_CATEGORIES = list.flatMap(CategoryService::loadAllCategoriesByPostIdFlux
      );
    }, 0, 10, TimeUnit.MINUTES);
  }

  private static Flux<Category> loadAllCategoriesByPostIdFlux(Category category) {

    return Flux.just(
      Category.builder()
        .id(category.getId())
        .categoryName(category.getCategoryName())
        .description(category.getDescription())
        .build(),
      Category.builder()
        .id(category.getId())
        .categoryName(category.getCategoryName())
        .description(category.getDescription())
        .build()
    );
  }
}
