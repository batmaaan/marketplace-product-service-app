package org.marketplace.product.service.data.repository;

import io.r2dbc.postgresql.api.PostgresqlConnection;
import io.r2dbc.postgresql.api.PostgresqlResult;
import lombok.AllArgsConstructor;
import org.marketplace.product.model.Product;
import org.marketplace.product.model.request.ProductRequest;
import org.marketplace.product.model.request.UpdateProductRequest;
import org.marketplace.product.model.utils.Order;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
@AllArgsConstructor
public class ProductRepository {
  private final Mono<PostgresqlConnection> postgresqlConnection;
  public static final String ID = "id";
  public static final String CATEGORY_ID = "category_id";
  public static final String PRODUCT_NAME = "product_name";
  public static final String DESCRIPTION = "description";
  public static final String PRICE = "price";

  public Mono<Product> getProductById(Integer id) {
    return postgresqlConnection.flatMapMany(connection -> connection.createStatement("select * from products where id=$1")
        .bind("$1", id)
        .execute())
      .flatMap(postgresqlResult -> postgresqlResult.map(s -> Product.builder()
        .id((Integer) s.get(ID))
        .categoryId((Integer) s.get(CATEGORY_ID))
        .productName((String) s.get(PRODUCT_NAME))
        .description((String) s.get(DESCRIPTION))
        .price((Integer) s.get(PRICE))
        .build()))
      .next();
  }

  public Flux<Product> getAllProducts(int limit, int offset, Order order) {
    return postgresqlConnection.flatMapMany(connection -> connection.createStatement(
          "SELECT * FROM products order by product_name " + order.toString() + " limit $1 offset $2 ")
        .bind("$1", limit)
        .bind("$2", offset)
        .execute())
      .flatMap(postgresqlResult -> postgresqlResult.map(s -> Product.builder()
        .id((Integer) s.get(ID))
        .categoryId((Integer) s.get(CATEGORY_ID))
        .productName((String) s.get(PRODUCT_NAME))
        .description((String) s.get(DESCRIPTION))
        .price((Integer) s.get(PRICE))
        .build()));

  }

  public Flux<Product> getProductsByCategory(Integer categoryId, int limit, int offset, Order order) {
    return postgresqlConnection.flatMapMany(connection -> connection.createStatement(
          "SELECT * FROM products where category_id=$1  order by product_name " + order.toString() +
            "  limit $2 offset $3")
        .bind("$1", categoryId)
        .bind("$2", limit)
        .bind("$3", offset)
        .execute())
      .flatMap(postgresqlResult -> postgresqlResult.map(s -> Product.builder()
        .id((Integer) s.get(ID))
        .categoryId((Integer) s.get(CATEGORY_ID))
        .productName((String) s.get(PRODUCT_NAME))
        .description((String) s.get(DESCRIPTION))
        .price((Integer) s.get(PRICE))
        .build()));
  }

  public Mono<Product> saveProduct(ProductRequest productRequest) {
    return postgresqlConnection.flatMapMany(connection -> connection.createStatement(
          "INSERT INTO  products (category_id, product_name, description, price) VALUES " +
            "($1, $2, $3, $4) returning id, category_id, product_name, description, price")
        .bind("$1", productRequest.getCategoryId())
        .bind("$2", productRequest.getProductName())
        .bind("$3", productRequest.getDescription())
        .bind("$4", productRequest.getPrice())
        .execute())
      .flatMap(postgresqlResult -> postgresqlResult.map(s -> Product.builder()
        .id((Integer) s.get(ID))
        .categoryId((Integer) s.get(CATEGORY_ID))
        .productName((String) s.get(PRODUCT_NAME))
        .description((String) s.get(DESCRIPTION))
        .price((Integer) s.get(PRICE))
        .build())).next();
  }

  public Mono<Product> updateProduct(UpdateProductRequest updateProductRequest) {
    return postgresqlConnection.flatMapMany(connection -> connection.createStatement(
          "UPDATE products set category_id=$1, product_name=$2, description=$3, price=$4 " +
            "WHERE id=$5 returning id, category_id, product_name, description, price")
        .bind("$1", updateProductRequest.getCategoryId())
        .bind("$2", updateProductRequest.getProductName())
        .bind("$3", updateProductRequest.getDescription())
        .bind("$4", updateProductRequest.getPrice())
        .bind("$5", updateProductRequest.getId())
        .execute())
      .flatMap(postgresqlResult -> postgresqlResult.map(s -> Product.builder()
        .id((Integer) s.get(ID))
        .categoryId((Integer) s.get(CATEGORY_ID))
        .productName((String) s.get(PRODUCT_NAME))
        .description((String) s.get(DESCRIPTION))
        .price((Integer) s.get(PRICE))
        .build())).next();
  }

  public Mono<Integer> deleteById(Integer id, Integer categoryId) {

      return postgresqlConnection.flatMapMany(connection -> connection.createStatement(
        "DELETE FROM products WHERE id=$1 and category_id=$2")
        .bind("$1", id)
        .bind("$2", categoryId)
        .execute()).flatMap(PostgresqlResult::getRowsUpdated).next();

  }
}
