package org.marketplace.product.service.api;

import lombok.AllArgsConstructor;
import org.marketplace.product.model.response.ProductResponse;
import org.marketplace.product.model.response.ProductsListResponse;
import org.marketplace.product.model.utils.Order;
import org.marketplace.product.service.data.service.ProductService;
import org.marketplace.product.model.request.ProductRequest;
import org.marketplace.product.model.request.UpdateProductRequest;
import org.marketplace.product.model.response.DeleteResponse;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.marketplace.product.model.utils.UrlEndpoints.*;

@RestController
@AllArgsConstructor
public class ProductController {

  private final ProductService productService;

  @GetMapping(value = GET_PRODUCT_BY_ID)
  public Mono<ProductResponse> getProductById(@RequestParam Integer id) {
    return productService.getProductById(id);
  }

  @GetMapping(GET_ALL_PRODUCTS)
  public Mono<ProductsListResponse> getAllProducts(@RequestParam int limit, @RequestParam int offset,
                                                   @RequestParam(defaultValue = "desc") Order order) {
    return productService.getAllProducts(limit, offset, order);
  }

  @GetMapping(GET_PRODUCT_BY_CATEGORY)
  public Mono<ProductsListResponse> getProductsByCategory(@RequestParam Integer categoryId, @RequestParam int limit, @RequestParam int offset,
                                                          @RequestParam(defaultValue = "desc") Order order) {
    return productService.getProductsByCategory(categoryId, limit, offset, order);
  }

  @PostMapping(SAVE_PRODUCT_URI)
  public Mono<ProductResponse> saveProduct(@RequestBody ProductRequest productRequest) {
    return productService.saveProduct(productRequest);
  }

  @PostMapping(UPDATE_PRODUCT_URI)
  public Mono<ProductResponse> updateProduct(@RequestBody UpdateProductRequest updateProductRequest) {
    return productService.updateProduct(updateProductRequest);
  }

  @DeleteMapping(DELETE_PRODUCT_BY_ID_URI)
  public Mono<DeleteResponse> deleteById(@RequestParam Integer id, @RequestParam Integer categoryId) {
    return productService.deleteById(id, categoryId);
  }
}

