package org.marketplace.product.service.data.repository;

import io.r2dbc.postgresql.api.PostgresqlConnection;
import lombok.AllArgsConstructor;
import org.marketplace.product.model.Category;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
@AllArgsConstructor
public class CategoryRepository {
  private final Mono<PostgresqlConnection> postgresqlConnection;

  public static final String ID = "id";
  public static final String CATEGORY_NAME = "category_name";
  public static final String DESCRIPTION = "description";

  public Mono<Category> getCategoryById(Integer id) {
    return postgresqlConnection.flatMapMany(connection -> {
        return connection.createStatement("select * from categories where id=$1")
          .bind("$1", id)
          .execute();
      })
      .flatMap(postgresqlResult -> {
        return postgresqlResult.map(s -> {

          return Category.builder()
            .id((Integer) s.get(ID))
            .categoryName((String) s.get(CATEGORY_NAME))
            .description((String) s.get(DESCRIPTION))
            .build();
        });
      })
      .next();
  }

  public Flux<Category> getAllCategories() {
    return postgresqlConnection.flatMapMany(connection -> {
      return connection.createStatement("SELECT * FROM categories")
        .execute();
    }).flatMap(postgresqlResult -> {
      return postgresqlResult.map(s -> {

        return Category.builder()
          .id((Integer) s.get(ID))
          .categoryName((String) s.get(CATEGORY_NAME))
          .description((String) s.get(DESCRIPTION))
          .build();
      });
    });
  }
}
