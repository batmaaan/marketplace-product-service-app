create sequence products_id_seq;
create table products

(
    id           integer default nextval('products_id_seq'::regclass) not null
        primary key,
    category_id   integer,
    product_name varchar(500)                                         not null,
    description  varchar(500)                                         not null,
    price        integer                                              not null
);
