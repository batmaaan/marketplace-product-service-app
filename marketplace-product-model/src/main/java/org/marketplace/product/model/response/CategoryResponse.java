package org.marketplace.product.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.product.model.Category;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryResponse {
  private String status;
  private String description;
  private Category category;
}
