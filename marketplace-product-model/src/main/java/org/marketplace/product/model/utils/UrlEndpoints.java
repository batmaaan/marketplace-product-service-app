package org.marketplace.product.model.utils;

public class UrlEndpoints {

  private static final String PRODUCT_URI = "/products";
  private static final String CATEGORY_URI = "/categories";

  public static final String GET_PRODUCT_BY_ID = PRODUCT_URI + "/by-id";
  public static final String GET_ALL_PRODUCTS = PRODUCT_URI + "/all";
  public static final String GET_PRODUCT_BY_CATEGORY = PRODUCT_URI + "/by-category";
  public static final String SAVE_PRODUCT_URI = PRODUCT_URI + "/save";
  public static final String UPDATE_PRODUCT_URI = PRODUCT_URI + "/update";
  public static final String DELETE_PRODUCT_BY_ID_URI = PRODUCT_URI + "/delete";

  public static final String GET_CATEGORY_BY_ID_URI = CATEGORY_URI + "/by-id";

  public static final String GET_ALL_CATEGORIES_URI = CATEGORY_URI + "/all";



}
