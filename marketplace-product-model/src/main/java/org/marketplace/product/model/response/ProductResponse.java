package org.marketplace.product.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.product.model.Product;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {
  private String status;
  private String description;
  private Product product;
}
