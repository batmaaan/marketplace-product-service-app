package org.marketplace.product.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.product.model.Category;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoriesListResponse {
  private String status;
  private List<Category> categories;
}
