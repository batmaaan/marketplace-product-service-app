package org.marketplace.product.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.product.model.Product;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductsListResponse {
  private String status;
  private String description;
  private boolean hasNext;
  private List<Product> products;
}
