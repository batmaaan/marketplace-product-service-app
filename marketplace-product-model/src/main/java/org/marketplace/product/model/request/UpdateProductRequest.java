package org.marketplace.product.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateProductRequest {
  private Integer id;
  private Integer categoryId;
  private String productName;
  private String description;
  private Integer price;
}
