mvn clean compile deploy
mvn compile -pl marketplace-product-service -U com.google.cloud.tools:jib-maven-plugin:3.2.1:build -Djib.to.auth.username=$DOCKER_USERNAME -Djib.to.auth.password=$DOCKER_PASS  -Dimage=batmaaan/marketplace-product-service:1.0-SNAPSHOT
cd /home/finch/app/products; docker-compose pull; docker-compose stop; docker-compose up -d;
